const express = require("express");
const app = express();
const routes = require("./routes");
const PORT = 3000;

app.use(express.static("views/assets")); //membuka file statis css, javascript
app.set("view engine", "ejs"); //view engine yang digunakan berupa ejs, bisa juga pug atau html
app.use(express.urlencoded({ extended: true })); //middleware untuk kirim2 data
app.use(express.json()); //menerima data json

app.use(routes);

app.listen(PORT, () => {
  console.log(`http://localhost:${PORT}`);
});
