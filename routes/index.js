const route = require("express").Router();
const { UserController, RoomController } = require("../controller");
const middleware = require("../middleware");

route.post("/register", UserController.register);
route.post("/login", UserController.login);
route.get("/users", middleware.authorization, UserController.getUsers);

//view
route.get("/login-page", UserController.loginPage);
route.get("/", middleware.authorization, UserController.homePage);

//Room Controller
route.post("/room", RoomController.createRoom);
route.post(
  "/fight/:id",
  middleware.authentication,
  RoomController.registerRoom
);

module.exports = route;
