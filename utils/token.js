const jwt = require("jsonwebtoken");
const secret = "asdfghijlk";

class Token {
  static generateToken(payload) {
    try {
      console.log(payload);
      const token = jwt.sign(payload, secret);
      console.log(token);
      return token;
    } catch (error) {
      console.log(error);
    }
  }

  static decodeToken(token) {
    try {
      const payload = jwt.verify(token, secret);
      return payload;
    } catch (error) {
      return null;
    }
  }
}

module.exports = Token;
