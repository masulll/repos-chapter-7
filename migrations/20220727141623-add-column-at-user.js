"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    queryInterface.addColumn("Users", "role", { type: Sequelize.STRING });
    //menggunakan Users karena untuk tabelname, param ke-2 nama row
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.(kebalikan dari command di up function)
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    queryInterface.removeColumn("Users", "role", { type: Sequelize.STRING });
  },
};
