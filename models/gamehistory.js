"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class GameHistory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      GameHistory.belongsTo(models.Room, { foreignKey: "RoomId" });
      GameHistory.belongsTo(models.User, { foreignKey: "winner" });
    }
  }
  GameHistory.init(
    {
      RoomId: DataTypes.INTEGER,
      winner: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "GameHistory",
    }
  );
  return GameHistory;
};
