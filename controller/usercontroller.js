const { User } = require("../models");
const { Token } = require("../utils");
class UserController {
  static async register(req, res, next) {
    try {
      console.log(req.body);
      const { name, password, role } = req.body;

      if (password.length < 6) {
        //cek kondisi data login
        res.status(400).json({
          msg: "Password anda kurang dari 6 karakter",
        });
      } else {
        const payload = {
          //construct agar lebih rapih dan tidak panjang saat create
          name,
          password,
          role,
        };

        const newUser = await User.create(payload);
        console.log(newUser, "ini newuser");
        res.status(200).json({
          msg: "Register Berhasil, silahkan login",
        });
      }
    } catch (error) {
      res.status(200).json({
        msg: "Something wrong, cannot register",
      });
      console.log(error);
    }
  }

  static async login(req, res, next) {
    try {
      const { name, password } = req.body; //ketika user memasukan nama dan password
      console.log(req.headers, "<<< 1");

      const loginUser = await User.findOne({
        //mencocokan name yang dimasukkan dengan name yang di db
        where: {
          name: name,
        },
      });
      const _id = loginUser.dataValues.id;
      const _password = loginUser.password; //variabel dari password db
      if (password !== _password) {
        //cek kesamaan password
        res.status(401).json({
          msg: "name atau password salah !",
        });
      } else {
        const _role = loginUser.dataValues.role;
        const token = Token.generateToken({ name, role: _role, id: _id });
        // window.localStorage.setItem("token", token); //save token ke localstorage
        if (token) {
          req.headers.token = token; //untuk menambahkan token pada headers
          // res.redirect("/" + "?token=" + token); // salah satu cara untuk merender payload karena token dikirim lewat headers (MVC)
          console.log(req.headers, "<<< 2");
          res.status(200).json({ token });
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  static async getUsers(req, res, next) {
    try {
      const userLogin = req.headers.userLogin;
      console.log(userLogin);
      const users = await User.findAll();
      res.status(200).json(users);
    } catch (error) {
      console.log(error);
    }
  }

  static async loginPage(req, res, next) {
    try {
      res.render("login");
    } catch (error) {
      console.log(error);
    }
  }

  static async homePage(req, res, next) {
    try {
      console.log(req.loginUser, "<<<");
      res.render("home", { user: req.headers.userLogin }); //token digunakan untuk create game karena informasi user didapatkan darisini
    } catch (error) {
      console.log(error);
    }
  }
}

module.exports = UserController;
