const { Room, UserRoom } = require("../models");
class RoomController {
  static async createRoom(req, res, next) {
    try {
      const { name } = req.body;
      await Room.create({ name });
      res.status(200).json({
        msg: "berhasil membuat room",
      });
    } catch (error) {
      console.log(error);
    }
  }

  static async registerRoom(req, res, next) {
    try {
      const { id } = req.headers.userLogin; //agar diakses setelah middleware
      const room_id = req.params.id; //param id room nya

      await UserRoom.create({ UserId: id, RoomId: room_id });
      res.status(200).json({
        msg: `Selamat datang di room${room_id}`,
      });
    } catch (error) {
      console.log(error);
    }
  }
}

module.exports = RoomController;
