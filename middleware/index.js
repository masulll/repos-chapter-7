const { Token } = require("../utils");

class middleware {
  static authentication(req, res, next) {
    try {
      const token = req.headers.token || req.body.token; //berasal dari header apakah ada token

      if (token) {
        const payload = Token.decodeToken(token);

        req.headers.userLogin = payload; //untuk room controller supaya dapet idnya

        if (payload) {
          next();
        } else {
          res.status(401).json({ msg: "Unauthorized access" }); //karena gaada token
        }
      } else {
        res.status(401).json({ msg: "Unauthorized access" });
      }
    } catch (error) {
      res.status(500).json({
        msg: "Something wrong",
      });
    }
  }

  static authorization(req, res, next) {
    try {
      const token = req.headers.token || req.query.token; //berasal dari header apakah ada token
      const payload = Token.decodeToken(token);
      console.log(payload);
      const _role = payload && payload.role ? payload.role : null; //ternary
      console.log(_role);
      if (_role === "SA") {
        //cek role
        req.headers.userLogin = payload;
        next();
      } else {
        res.status(401).json({ msg: "Unauthorized access" });
      }
    } catch (error) {
      console.log(error);
      res.status(500).json({
        msg: "Something wrong",
      });
    }
  }
}

module.exports = middleware;
